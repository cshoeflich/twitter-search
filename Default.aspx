﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Twitter Search</title>
    <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="CSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="JS/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="JS/TwitterSearch.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" EnablePageMethods="true" runat="server" />
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2>Twitter Search</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 input-group" style="margin-left: 20px;">
                    <span class="input-group-addon">#</span>
                    <input id="txtSearchText" type="text" class="form-control" placeholder="Search for..." title="Search twitter for popular tweets containing the specified hashtag" />
                    <span class="input-group-btn">
                        <button id="btnSearch" type="button" class="btn btn-primary" onclick="btnSearch_ClientClick()">Search</button>
                    </span>
                </div>
                <div class="col-md-8"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="tableSearchResults" class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-md-6">Tweet</th>
                                <th class="col-md-2">Author</th>
                                <th class="col-md-2">Created</th>
                                <th class="col-md-2">Statistics</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul id="pagingContainer" class="pagination"></ul>
                </div>
                <input id="recordsPerPage" type="hidden" value="5" />
            </div>
        </div>
    </form>
</body>
</html>
