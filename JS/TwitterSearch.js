﻿$(document).ready(function () {

    // trigger search if enter key is clicked
    $(document).bind('keypress', function (e) {
        if (e.keyCode == 13) {
            btnSearch_ClientClick();
            return false;
        }
    });
});

function btnSearch_ClientClick() {
    var searchText = $.trim($("#txtSearchText").val());
    
    if(!inputIsValid(searchText)) {
        return false;
    }

    $.ajax({
        type: "POST",
        url: "Default.aspx/TwitterSearch",
        data: '{"searchText": "' + searchText + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            alert(response.d);
        }
    });
}

function inputIsValid(inputText) {

    if (inputText == "") {
        alert("Please enter a search term");
        return false;
    }
    else if(inputText.match("^#")) {
        alert("Please do not enter a hashtag at the start of the search value");
        return false;
    }
    return true;
}

function OnSuccess(response) {
    var searchResults = $.parseJSON(response.d);

    if (searchResults.statuses.length == 0) {
        alert("No tweets found with that hashtag");
        return false;
    }

    // remove existing rows in the table
    $("#tableSearchResults").find("tbody tr").remove();

    initiatePaging(searchResults);

    // add rows to table
    $.each(searchResults.statuses, function (index, searchResult) {
        $("#tableSearchResults").find("tbody")
            .append($("<tr>")
                .append($("<td>")
                    .append(searchResult.text)
                    .append(" (")
                    .append($("<a>")
                        .attr("href", formatLink(searchResult))
                        .attr("target", "_blank")
                        .text("link")
                    )
                    .append(")")
                )
                .append($("<td>")
                    .append($("<img>")
                        .attr("src", searchResult.user.profile_image_url)
                    )
                    .append(searchResult.user.name)
                )
                .append($("<td>")
                    .append(formatDate(searchResult.created_at))
                )
                .append($("<td>")
                    .append($("<img>")
                        .attr("src", "Images/retweet.png")
                        .css("width", "25px")
                        .css("height", "25px")
                    )
                    .append("&nbsp;")
                    .append(searchResult.retweet_count)
                    .append("&nbsp;")
                    .append($("<img>")
                        .attr("src", "Images/favorite.png")
                        .css("width", "25px")
                        .css("height", "21px")
                    )
                    .append("&nbsp;")
                    .append(searchResult.favorite_count)
                )
            );
    });

    // start by only showing the first page
    selectPage(1);
}

function formatDate(unformattedDate) {
    var date = new Date(unformattedDate);

    var month = padWithZero(date.getMonth() + 1);
    var day = padWithZero(date.getDate());
    var year = date.getFullYear();

    var hours = padWithZero(date.getHours());
    var minutes = padWithZero(date.getMinutes());
    var seconds = padWithZero(date.getSeconds());

    return month + "/" + day + "/" + year + "&nbsp;" + hours + ":" + minutes + ":" + seconds;
}

function padWithZero(inputNumber) {
    if (inputNumber < 10) {
        return "0" + inputNumber;
    }
    return inputNumber;
}

function initiatePaging(searchResults) {

    var recordsPerPage = parseInt($("#recordsPerPage").val());
    var numberSearchResults = searchResults.statuses.length;
    var numberPages = Math.ceil(numberSearchResults / recordsPerPage);

    // remove current paging links and create new ones
    var pagingContainer = $("#pagingContainer");
    $(pagingContainer).find("li").remove();
    for (var i = 1; i <= numberPages; i++) {
        $(pagingContainer).append($("<li>")
            .addClass("page-item")
            .append($("<a>")
                .addClass("page-link")
                .attr("href", "javascript:selectPage(" + i + ")")
                .text(i)
            )
        );
    }
}

function selectPage(pageNumber) {
    
    var recordsPerPage = parseInt($("#recordsPerPage").val());

    // find which record to start/end on
    var startingRecord = (pageNumber - 1) * recordsPerPage;
    var endingRecord = startingRecord + recordsPerPage;

    // hide all table records
    $("#tableSearchResults").find("tbody>tr").hide();

    // only show the records for the selected page
    $("#tableSearchResults").find("tbody>tr").slice(startingRecord, endingRecord).show();

    // add styling to the current selected page to show it is selected
    $("#pagingContainer").find("li").removeClass("active");
    $("#pagingContainer").find('li:contains("' + pageNumber + '")').addClass("active");
}

function formatLink(searchResult) {
    var link = "http://twitter.com/";
    link += searchResult.user.screen_name + "/status/";
    link += searchResult.id_str;

    return link;
}